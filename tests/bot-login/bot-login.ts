import { expect } from "chai";
import * as path from "path";
import Bot from "../../src/Bot";

describe("Bot", () => {
    it("Login connection ", (done) => {
        const root = path.join(__dirname, "../../../");

        console.log("env", process.env);
        console.log("env token", process.env.TEST_API_TOKEN);
        const newBot = new Bot(root.toLocaleString(), process.env.TEST_API_TOKEN);

        newBot.Init();
        newBot.LoginClient()
            .then((result) => {
                const e = expect(result).to.be.empty;
                done();
            })
            .catch((reason) => { console.error(reason); });
    }).timeout(5000);
});

process.exit();
