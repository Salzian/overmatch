
# ![Overmatch Logo](http://fabian-fritzsche.de/overmatch/OverMatch-32.png) Overmatch

[![pipeline status](https://gitlab.com/Salzian/overmatch/badges/master/pipeline.svg)](https://gitlab.com/Salzian/overmatch/commits/master)

## Overview

  [Features](#what-is-overmatch)  
  [Development](#development)  
  [Contributing](#contributing)  
  [Problems](#problems)  
  [Contact](#contact)  

## Features

OverMatch is a [Discord](https://discordapp.com) bot to manage custom matchmaking events for [Overwatch](https://playoverwatch.com/).

### Features include:

**Disclaimer:** These are planned features and may not be (fully) implemented yet. More information: [Current Development](https://gitlab.com/Salzian/overmatch/boards) and [Roadmap](https://gitlab.com/Salzian/overmatch/milestones)

- Automatic mode
  - Players registering in a queue will be automatically matched by their corresponding skill rating (SR)
  - OverMatch checks if each player accepts the invitation
  - Players will automatically be moved or invited into dedicated, generated voice channels
- Supervised mode
  - Mode suited for streamers and events
  - Admin can move players from the queue into corresponding teams
- Manual mode
  - Players can create their own matches which other players can apply for joining them


## Development

Overmatch is currently in active development and not available to the public. As soon as a stable release is ready for beta testing, a Discord server with a bot runnning a development build will be setup and provided here.

As soon as the bot reaches a stable production phase you will be able to invite the bot to your server.

If you want to find more about the current state of development, have a look here:

[Current Development](https://gitlab.com/Salzian/overmatch/boards)  
[Development Roadmap](https://gitlab.com/Salzian/overmatch/milestones)

## Contributing

If you want to contribute to the development of Overmatch, please [send me a message](#contact).

The bot is written in [TypeScript](https://www.typescriptlang.org/) using [Node.js](https://nodejs.org/en/) and [discord.js](https://discord.js.org/).

Knowledge in TypeScript is a plus otherwise JavaScript must be decently familiar. Advanced knowledge about Node.js is also required.

## Problems

If you encounter an issue, head over to the [issue's page](https://gitlab.com/Salzian/overmatch/issues).

## Contact

In case you want to contact me...

|               |                                                                   |
|   ------:     |   -----------------------------------------------------------     |
|   Discord     |   Salzian#0001                                                    |
|   Email       |   [mail@fabian-fritzsche.de](mailto:mail@fabian-fritzsche.de)     |
|   Twitter     |   [@SaltySalzian](https://twitter.com/SaltySalzian)               |
