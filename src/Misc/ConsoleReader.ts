import * as Event from "events";

/**
 * Reads console input for debugging purposes and server commands.
 * @event input Called on console input, returns input string.
 */
export default class ConsoleReader {
    public consoleEvent: Event.EventEmitter = new Event.EventEmitter();

    /**
     * Adds listener to console input and sets up event.
     */
    constructor() {
        const stdin = process.openStdin();

        stdin.addListener("data", (data) => {
            const input: string = data.toString().trim();

            this.consoleEvent.emit("input", input);
        });
    }
}
