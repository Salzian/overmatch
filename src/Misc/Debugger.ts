import Bot from "../Bot";
import ConsoleReader from "./ConsoleReader";

/**
 * Helper class for debugging purposes
 */
export class Debugger {
    public bot: Bot;
    public consoleReader: ConsoleReader;

    constructor(bot: Bot, consoleReader: ConsoleReader) {
        this.bot = bot;
        this.consoleReader = consoleReader;
        consoleReader.consoleEvent.on("input", (input) => {
            if (input === "reset") {
                bot.Reset();
            }
        });
    }
}
