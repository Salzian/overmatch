/**
 * Helper class with general purpose utility functions
 */
export class Util {

    /**
     * Checks if given instance already equals sender.
     * @param sender Sender class of which to check the singleton.
     * @param instance Singleton reference
     * @returns Returns the sender if `instance == null`.
     */
    public static SetInstance(sender: any, instance: any): any {
        console.log("Util.SetInstance()", "Setting instance of", sender, "...");
        if (instance == null) {
            return sender;
        } else {
            console.error("Util.SetInstance", "Double instance");
        }
    }
}
