import { Guild, Presence, User } from "discord.js";

export class Matcher {
    protected guild: Guild;
    protected isPrivate: boolean;
    protected playerQueue: User[];

    public constructor(guild: Guild) {
        this.guild = guild;
    }

    public AddPlayerToQueue(user: User) {
        this.playerQueue.push(user);
    }

    public RemovePlayerFromQueue(user: User) {
        const index = this.playerQueue.indexOf(user);
        this.playerQueue.splice(index, 1);
    }

    public GetNextPlayerFromQueue(): User {
        return this.playerQueue.splice(0, 1)[0];
    }
}
