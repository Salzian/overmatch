import { Guild } from "discord.js";
import { Util } from "../Misc/Utility";
import { Matcher } from "./Matcher";

/**
 * Manages Matcher
 */
export class OverMatchManager {
    private static instance: OverMatchManager = null;
    private static matchers: Matcher[];
    /**
     * Array of currently active Matchers
     */
    public get Matchers() {
        return OverMatchManager.matchers;
    }

    constructor() {
        OverMatchManager.instance = Util.SetInstance(this, OverMatchManager.instance);
    }

    /**
     * Instantiate new Matcher
     * @param guild Guild to add Matcher to
     * @returns new Matcher
     */
    public CreateMatchMaker(guild: Guild): Matcher {
        return OverMatchManager.matchers[guild.id] = new Matcher(guild);
    }

    /**
     * Destroys Matcher by given Guild
     * @param guild Guild of with the Matcher should be destroyed
     */
    public DestroyMatchMakerByGuild(guild: Guild) {
        const index = OverMatchManager.matchers[guild.id];
        delete OverMatchManager.matchers.splice(index, 1)[0];
    }

    /**
     * Destroys Matcher
     * @param matcher Matcher that should be destroyed
     */
    public DestroyMatchMakerByMatcher(matcher: Matcher) {
        const index = OverMatchManager.matchers.indexOf(matcher);
        delete OverMatchManager.matchers.splice(index, 1)[0];
    }
}
