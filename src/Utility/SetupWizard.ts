import { CategoryChannel, Guild, Message, TextChannel } from "discord.js";
import * as fs from "fs";
import * as path from "path";
import Bot from "../Bot";
import { ISettings, Settings } from "./Settings";

/**
 * Setup wizard to setup OverMatch for a server.
 * Also saves and reads save files.
 */
export class SetupWizard {

    /**
     * Deletes JSON setting file of given guild.
     * @param guild Guild of which the setting file should be deteted.
     */
    public static DeleteSaveFile(bot: Bot, guild: Guild) {
        fs.unlink(path.join(bot.Root, `/data/servers/${guild.id}.json`), (err) => { throw err; });
    }

    /**
     * Saves settings as JSON file.
     * @param settings Settings to save.
     */
    private static SaveSettings(bot: Bot, settings: Settings): Promise<any> {
        const settingsJSON = JSON.stringify(settings);

        return new Promise((resolve, reject) => {
            try {
                fs.writeFileSync(path.join(bot.Root, `/data/servers/${settings.guildID}.json`), settingsJSON);
                resolve();
            } catch (error) {
                console.error("SetupWizard.StartSetup()", error);
            }
        });

    }

    /**
     * Starts the setup process for the given guild.
     * Messages guild owner to setup the server.
     * @param guild Guild to start setup at.
     */
    public async StartSetup(bot: Bot, guild: Guild) {
        const settings = new Settings();
        let sentMessage;

        console.log(
            "SetupWizard.StartSetup()",
            "Starting setup for",
            guild.name, "by",
            guild.owner.displayName + "#" + guild.owner.user.discriminator);

        settings.guildID = guild.id;
        settings.guildOwner = guild.ownerID;

        await guild.owner.user.send("Welcome to OverMatch. To start using OverMatch a short setup is required.")
            .then((message) => {
                sentMessage = message as Message;
            })
            .catch((reason) => { console.error("SetupWizard.StartSetup()", reason); });

        await sentMessage.reply("Please provide a prefix of your choice:")
        .catch((reason) => { console.error(reason); });

        await sentMessage.channel.awaitMessages((sender) => sender.author !== Bot.Instance.Client.user, { max: 1 })
            .then((collected) => {
                settings.prefix = collected.first().content;
            })
            .catch((reason) => { console.error("SetupWizard.StartSetup()", reason); });

        // tslint:disable-next-line:max-line-length
        await sentMessage.reply("OverMatch needs to create some channels now. You can move and rename the channels afterwards. Click ✔ to continue.")
            .then(async (sent) => {
                await sent.react("✔")
                    .then(async () => {
                        const filter = (reaction, sender) => {
                            reaction.emoji.name === "✔" && sender.id === guild.owner.id;
                        };

                        await sent.awaitReactions(filter, { max: 1 })
                        .catch((reason) => { console.error("SetupWizard.StartSetup()", reason); });
                    }).catch((reason) => { console.error("SetupWizard.StartSetup()", reason); });
            }).catch((reason) => { console.error("SetupWizard.StartSetup()", reason); });

        //#region Setup Channels
        let adminConsole: TextChannel;
        let category: CategoryChannel;
        let dashboard: TextChannel;
        await guild.createChannel("overmatch", "category").then((channel) => {
            category = channel as CategoryChannel;
        }).catch((reason) => { console.error(reason); });

        await guild.createChannel("overmatch-console", "text").then((channel) => {
            adminConsole = channel as TextChannel;
        }).catch((reason) => { console.error(reason); });

        await guild.createChannel("overmatch", "text").then((channel) => {
            dashboard = channel as TextChannel;
        }).catch((reason) => { console.error(reason); });

        dashboard.setParent(category);
        adminConsole.setParent(category);

        settings.overmatchChannels.category = category.id;
        settings.overmatchChannels.dashboard = dashboard.id;
        settings.overmatchChannels.adminConsole = adminConsole.id;

        const finalSettings = new Settings(settings as ISettings);
        //#endregion

        let result;
        await SetupWizard.SaveSettings(bot, finalSettings).then(() => result = true).catch((error) => result = error);

        if (result === true) {
            await sentMessage.reply("Setup completed! Have fun!")
            .catch((reason) => { console.error("SetupWizard.StartSetup()", reason); });
        } else {
            await sentMessage.reply("An error occured: ```" + result.code + "```")
            .catch((reason) => { console.error("SetupWizard.StartSetup()", reason); });
        }
    }
}
