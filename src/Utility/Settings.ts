/**
 * Data class to save settings
 */
export class Settings {
    public prefix: string = "";
    public guildID: string = "";
    public guildOwner: string = "";
    public overmatchChannels: {
        category: string
        dashboard: string
        adminConsole: string,
    } = {
            adminConsole: "",
            category: "",
            dashboard: "",
        };

    constructor(object?: ISettings) {
        if (object === undefined) {
            return;
        }

        this.prefix = object.prefix;
        this.guildID = object.guildID;
        this.guildOwner = object.guildOwner;
        this.overmatchChannels = {
            adminConsole: object.overmatchChannels.adminConsole,
            category: object.overmatchChannels.category,
            dashboard: object.overmatchChannels.dashboard,
        };
    }
}

export interface ISettings {
    prefix: string;
    guildID: string;
    guildOwner: string;
    overmatchChannels: {
        adminConsole: string,
        category: string,
        dashboard: string,
    };
}
