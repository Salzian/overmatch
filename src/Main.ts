
import * as path from "path";
import Bot from "./Bot";
import ConsoleReader from "./Misc/ConsoleReader";
import { Debugger } from "./Misc/Debugger";

// Get CLI without two first elements
const ARGS = process.argv.slice(2);

// Set root
const ROOT = SetRoot();

const TOKEN = ReadArgs(ARGS);

InstantiateBot()
    .then(async (bot) => {
        await bot.Init();
        await bot.LoginClient().catch((reason) => { console.error("Main", reason); process.exit(1); });
        const debug = new Debugger(bot, consoleReader);
    })
    .catch((reason) => {
        console.error("Main", "Cannot login bot", reason);
        process.exit(1);
    });

const consoleReader = new ConsoleReader();

function InstantiateBot(): Promise<Bot> {
    return new Promise<any>((resolve, reject) => {
        let bot: Bot;
        try {
            if (TOKEN !== undefined) {
                console.log("Main.InstantiateBot()", "API token via CLI", TOKEN);
                bot = new Bot(ROOT, TOKEN);
                resolve(bot);
            } else {
                bot = new Bot(ROOT);
                resolve(bot);
            }
        } catch (error) {
            console.log("Main.InstantiateBot()", "Bot was not instantiated", error);
            reject(error);
        }
    });
}

function ReadArgs(args: string[]) {
    const tokenIndex = args.indexOf("api-token", 0);
    const token = args[tokenIndex + 1];
    return token;
}

function SetRoot() {
    const root = path.join(__dirname, "../../");
    console.log("Main.SetRoot()", "Project root", root.toString());
    return root;
}
