import { Client, Collection, Guild } from "discord.js";
import { CategoryChannel } from "discord.js";
import * as fs from "fs";
import * as path from "path";
import { OverMatchManager } from "./MatchMaking/OverMatchManager";
import { Util } from "./Misc/Utility";
import { Settings } from "./Utility/Settings";
import { SetupWizard } from "./Utility/SetupWizard";

/**
 * Main logic class of bot.
 * Handles communication and events from the client.
 */
export default class Bot {
    //#region Fields
    // Instance
    private static instance: Bot = null;
    public static get Instance() {
        if (Bot.instance == null) {
            Bot.instance = Util.SetInstance(this, this.Instance);
        }
        return Bot.instance;
    }

    // Properties
    private client: Client;
    public get Client(): Client {
        return this.client;
    }

    private root: string;
    public get Root(): string {
        return this.root;
    }

    // Privates
    private guildConfigurations: Collection<Guild, Settings> = new Collection<Guild, Settings>();
    private overMatchManager: OverMatchManager;
    private token: string;
    private isInit: boolean = false;
    //#endregion

    /**
     * Constructs new bot instance and logs in to Discord
     * @param root Root directory
     * @param token API token to use for login
     */
    constructor(root: string, token?: string) {
        this.root = root;

        Bot.instance = Util.SetInstance(this, Bot.instance);

        this.client = new Client();
        this.overMatchManager = new OverMatchManager();
    }

    //#region Publics

    public Init(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.MakeDirectories();
            this.RegisterEvents();

            if (this.token === undefined) {
                this.ReadToken()
                    .then((result) => this.token = result)
                    .catch(() => {
                        // tslint:disable-next-line:max-line-length
                        console.error("Bot()", "Please provide an Discord API token in root/settings.jsonor use `npm start api-token [API_TOKEN]`");
                        process.exit(1);
                    });
            }

            this.isInit = true;
            resolve();
        });
    }

    /**
     * Reads API token and tries to login.
     * @returns `Promise<string>` containing either nothing on success or an error message
     */
    public LoginClient(): Promise<string> {

        console.log("Bot.LoginClient()", "Logging in client...");
        return new Promise<string>((resolve, reject) => {

            if (this.isInit === false) {
                console.error("Bot.LoginClient()", "Bot not initialized yet");
                reject("Bot not initialized yet");
                return;
            }

            console.log("Bot.LoginClient()", "token", this.token);
            this.Client.login(this.token)
                .then(() => {
                    console.log("Bot.LoginClient()", "Client logged in.");
                    resolve();
                }).catch((reason) => { console.error("Bot.LoginClient()", reason); });
        });
    }

    /**
     * Deletes all generated channels.
     */
    // TODO DEBUG remove before release
    public Reset() {
        const guildArray = this.Client.guilds.array();

        // Cycle through all guilds
        for (const guild of guildArray) {
            // Get all overmatch categories
            const categories = guild.channels.filter(
                (channel) => channel.name.toLowerCase().startsWith("overmatch") && channel.type === "category");

            // Cycle found categories
            categories.forEach(async (category) => {
                const categoryChannel = category as CategoryChannel;

                // Delete all children of category
                await categoryChannel.children.forEach(async (child) => {
                    await child.delete();
                });

                await category.delete();
            });
        }
    }

    //#endregion

    //#region Privates
    /**
     * Runs after successful login.
     */
    private AfterLoginCode() {
        this.CheckGuilds();
    }

    /**
     * Sets up all needed directories e.g. save file directory.
     */
    private MakeDirectories() {
        if (fs.existsSync(path.join(this.Root, "data/servers"))) {
            return;
        } else {
            fs.mkdir(path.join(this.Root, "data/servers"), (error) => console.error("Bot.MakeDirectories()", error));
        }
    }

    /**
     * Registers all necessary `Bot.Client` events.
     */
    private RegisterEvents() {
        // Bot Events
        this.Client.on("ready", () => {
            console.log("Bot", "Logged in as", this.Client.user.tag);
            console.log("Bot", "Connected to", this.Client.guilds.size, "server(s).");

            this.AfterLoginCode();
        });

        this.Client.on("guildCreate", (guild) => {
            console.log("Bot", "Bot was invited to new server", guild.id, guild.name);
            const wizard = new SetupWizard();
            wizard.StartSetup(this, guild);
        });

        this.Client.on("guildDelete", (guild) => {
            console.log("Bot", "Bot was removed from server", guild.id, guild.name);
            SetupWizard.DeleteSaveFile(this, guild);
            this.guildConfigurations.delete(guild);
        });

        this.Client.on("message", (message) => {
            if (this.guildConfigurations.size < 1) {
                return;
            }

            const settings = this.guildConfigurations.get(message.guild);

            if (message.channel.id !== settings.overmatchChannels.adminConsole) {
                return;
            }

        });
    }

    /**
     * Reads settings api token
     * @returns `Promise<string>` containing either the token or an error message
     */
    private ReadToken(): Promise<string> {
        console.log("Bot.ReadToken(): Reading API token from file...");
        return new Promise<string>((resolve, reject) => {
            try {
                let token: any = fs.readFileSync(path.join(this.Root, "settings.json"));
                token = JSON.parse(token);
                token = token["api-token"];
                console.log("Bot.ReadToken()", "API token found");
                resolve(token);
            } catch (error) {
                console.error("Bot.ReadToken()", "root/settings.json not found");
                reject(error);
            }
        });
    }

    /**
     * Checks all guilds
     */
    private CheckGuilds() {
        this.Client.guilds.forEach((guild) => {
            this.CheckGuild(guild);
            this.AddGuildListeners(guild);
        });
    }

    /**
     * Checks if the given guild has been set up yet. Starts `SetupWizard` if server is not setup yet.
     * @param guild Guild to check
     */
    private async CheckGuild(guild: Guild) {
        const fileLocation = path.join(this.Root, `data/servers/${guild.id}.json`);
        try {
            const file = fs.readFileSync(fileLocation);
            const settings = new Settings(JSON.parse(file.toString()));
            this.guildConfigurations.set(guild, settings);
        } catch (error) {
            if (error.code === "ENOENT") {
                console.log("Bot.CheckGuild()", `Save file ${fileLocation} not found. Generating new one...`);
                const wizard = new SetupWizard();
                await wizard.StartSetup(this, guild);
            } else {
                console.error("Bot.CheckGuild()", error);
            }
        }
    }

    /**
     * Adds listeners for specified guild
     * @param guild Guild to attach listeners to
     */
    private AddGuildListeners(guild: Guild) {
        console.error("Bot.AddGuildListeners()", "Not implemented!", "AddGuildListeners");
    }
    //#endregion
}
