#!/bin/bash

# any future command that fails will exit the script
set -e

# Kill screen
screen -X -S overmatch quit

# Delete the old repo
rm -rf /home/fabian/overmatch

# clone the repo again
git clone git@gitlab.com:Salzian/overmatch.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
PATH=/home/fabian/node/bin:$PATH
# source /home/fabian/.nvm/nvm.sh

# stop the previous pm2
##pm2 kill
##npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
##npm install pm2 -g
# starting pm2 daemon
##pm2 status

cd /home/fabian/overmatch

screen -d -m ./install.sh